module Main (main) where

import Lib
import Mhead
import Factorial
import Fibonacci

main :: IO ()
main = do
     let x = [1..10]
     putStrLn "Our list is:"
     print (x)
     putStrLn "The first element of the list is:"
     print (head x)
     putStrLn "The first element of the mhead1 list is:"
     print (mhead1 x)
     putStrLn "The first element of the mhead2 list is:"
     print (mhead2 x)
     putStrLn "The first element of the mhead3 list is:"
     print (mhead3 x)
     putStrLn "The first element of the mhead4 list is:"
     print (mhead4 x)
     putStrLn "The first element of the mhead5 list is:"
     print (mhead5 x)
     putStrLn "The first element of the mhead6 list is:"
     print (mhead6 x)
     putStrLn "Factorial test of 10:"
     print (mfact 10)
     putStrLn "Fibonacci test of 10:"
     print (fib 10)
     putStrLn "Fibonacci2 test of 10:"
     print (fib2 10)
     putStrLn "Q: What is (b:_) for?"
     putStrLn "Pattern matching, extracts the head element from the list and ignores the rest, binding that element to b"
     putStrLn "Q: What does the t@ do?"
     putStrLn "binds the entire matched list to 't'"