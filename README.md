# Lab3 - (Haskell)


## Learning objectives for Lab3, as outlined by lecturer

> - Basics of Haskell types
> - Basics of list processing in Haskell
> - Basic functions declarations


----------------



# The tasks

### Task 1:  Implementing mhead

Implementations of 6 mhead functions, following these requirements, respectively:

> - using pattern matching
> - using guards
> - single line using if..else.. expressions
> - using let..in..
> - using where
> - using case..of

#### Input:
```
[1..10]
```
#### Output:
Using head:
```
1
```
Using pattern matching:
```
1
```
Using function guards:
```
1
```
Using if else:
```
1
```
Using let in:
```
1
```
Using where:
```
1
```
Using case of:
```
1
```
--------------
### Task 2: Factorial

Write factorial function, called mfact that takes an integer, and
returns a factorial of this integer.

#### Input:
```
10
```
#### Output:
```
3628800
```
--------------
### Task 3: Fibonacci function
Write a recursive fib n function that takes an integer and returns the n-th fibonacci number.
#### Input:
```
10
```
#### Output:
```
55
```
--------------
### Task 4: Fibonacci function 2
```
fibs = 0 : 1 : next fibs
    where
        next (a : t@(b:_)) = (a+b) : next t
```
Write a new version of your fib n function, call it fib2 n that uses fib sequence defined above,
and returns the n-th element from that sequence.
#### Input:
```
10
```
#### Output:
```

55
```
#### Questions:
```
Q: What is (b:_) for?
A: Pattern matching, extracts the head element from the list and ignores the rest, binding that element to b
Q: What does the t@ do?
A: binds the entire matched list to 't'
```
