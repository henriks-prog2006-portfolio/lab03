module Fibonacci
    ( fib, fib2
    ) where

fib :: Integer -> Integer
fib n
  | n < 0     = error "Cannot be a negative number"
  | n == 0    = 0
  | n == 1    = 1
  | otherwise = fib (n - 1) + fib (n - 2)

-- | Calculate the n-th Fibonacci number.
--
-- >>> fib 0
-- 0
-- >>> fib 1
-- 1
-- >>> fib 2
-- 1
-- >>> fib 5
-- 5
-- >>> fib 10
-- 55
--
-- This function will throw an error for negative inputs:
--
-- >>> fib (-1)
-- *** Exception: Fibonacci is not defined for negative numbers

fib2 :: Int -> Integer
fib2 n = fibs !! n
  where
    fibs = 0 : 1 : next fibs
      where
        next (a : t@(b:_)) = (a + b) : next t


