module Factorial
    ( mfact
    ) where



mfact :: Integer -> Integer
mfact n
  | n < 0     = error "Cannot be a negative number"
  | n == 0    = 1
  | otherwise = n * mfact (n - 1)

-- | Calculate the factorial of a non-negative integer.
--
-- >>> mfact 0
-- 1
-- >>> mfact 5
-- 120
-- >>> mfact 10
-- 3628800
--
-- This function will throw an error for negative inputs:
--
-- >>> mfact (-1)
-- *** Exception: Factorial is not defined for negative numbers